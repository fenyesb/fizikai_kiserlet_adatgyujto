package bluetoothviz.aut.bme.hu;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.LineChart;

public class MainActivity extends Activity {

    private float PeriodMS = 300;
    private Visualizer Visualizer = null;
    private Bluetooth Bluetooth = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ((Button)findViewById(R.id.ScanButton)).setOnClickListener(ScanButtonOnClick);
        ((Button)findViewById(R.id.ConnectButton)).setOnClickListener(ConnectButtonOnClick);
        ((Button)findViewById(R.id.DisconnectButton)).setOnClickListener(DisconnectButtonOnClick);
        ((Button)findViewById(R.id.HideButton)).setOnClickListener(HideButtonOnClick);

        Visualizer = new Visualizer((LineChart)findViewById(R.id.chart1), (LineChart)findViewById(R.id.chart2), PeriodMS/1000);
        Visualizer.CreateCharts();

        BluetoothManager btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter BtAdapter = btManager.getAdapter();
        if (BtAdapter != null && !BtAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, 1);
        }
        Bluetooth = new Bluetooth(this, BtAdapter, (int)PeriodMS, Visualizer);
    }

    View.OnClickListener ScanButtonOnClick = new View.OnClickListener() {
        public void onClick(View v)
        {
            Bluetooth.Scan();
        }
    };

    View.OnClickListener ConnectButtonOnClick = new View.OnClickListener() {
        public void onClick(View v)
        {
            Bluetooth.Connect();

        }
    };

    View.OnClickListener HideButtonOnClick = new View.OnClickListener() {
        public void onClick(View v)
        {
            findViewById(R.id.ButtonsLayout).setVisibility(LinearLayout.GONE);
        }
    };

    View.OnClickListener DisconnectButtonOnClick = new View.OnClickListener() {
        public void onClick(View v)
        {
            Bluetooth.Disconnect();
        }
    };

    @Override
    public void onBackPressed() {
        LinearLayout lay = ((LinearLayout)findViewById(R.id.ButtonsLayout));
        if (lay.getVisibility() == LinearLayout.GONE)
            lay.setVisibility(LinearLayout.VISIBLE);
        else
            finish();
    }

}
