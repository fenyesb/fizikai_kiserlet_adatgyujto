package bluetoothviz.aut.bme.hu;

import android.graphics.Color;
import android.os.Handler;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

/**
 * Created by Nagy Ákos on 2016. 07. 15..
 */
public class Visualizer {
    private LineChart mTemperatureChart, mPressureChart;
    private float mPeriodTime;
    final Handler myHandler = new Handler();

    public Visualizer(LineChart chartTemp, LineChart chartPressure, float periodTine)
    {
        mTemperatureChart = chartTemp;
        mPressureChart = chartPressure;
        mPeriodTime = periodTine;
    }

    public void CreateCharts()
    {
        CreateChart(mTemperatureChart, "Temperature", Color.RED, 20.0f, 45.0f);
        CreateChart(mPressureChart, "Pressure", Color.BLUE, 100.0f, 350.0f);
    }

    private void CreateChart(LineChart chart, String name, int color, float min, float max)
    {
        chart.setDrawGridBackground(false);
        chart.setTouchEnabled(true);
        chart.setPinchZoom(true);
        chart.setDescription("");
        chart.getLegend().setEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(CreateDataSet(name, color));
        chart.setData(new LineData(dataSets));

        chart.getAxisLeft().setAxisMaxValue(max);
        chart.getAxisLeft().setAxisMinValue(min);
        chart.getAxisRight().setEnabled(false);
    }

    private LineDataSet CreateDataSet(String name, int color)
    {
        ArrayList<Entry> values = new ArrayList<Entry>();
        LineDataSet set = new LineDataSet(values, name);

        // set the line to be drawn like this "- - - - - -"
        set.enableDashedLine(10f, 5f, 0f);
        //set.enableDashedHighlightLine(10f, 5f, 0f);
        set.setLineWidth(1f);
        set.setCircleRadius(3f);
        set.setDrawCircleHole(false);
        set.setValueTextSize(0f);
        set.setDrawFilled(false);
        set.setColor(color);
        set.setCircleColor(color);
        set.setFillColor(color);

        set.setLabel("Test");

        return set;
    }

    public void AddPressurePoint(float val)
    {
        AddPoint(mPressureChart, val);
    }

    public void AddTemperaturePoint(float val)
    {
        AddPoint(mTemperatureChart, val);
    }

    private void AddPoint(final LineChart chart, float val)
    {
        if (chart.getData() != null) {
            LineDataSet set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            if (set1 != null) {
                if (set1.getEntryCount() == 0)
                    set1.addEntry(new Entry(0.0f, val));
                else
                    set1.addEntry(new Entry(set1.getXMax() + mPeriodTime, val));
                chart.getData().notifyDataChanged();
                chart.notifyDataSetChanged();
                myHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        chart.invalidate();
                    }
                });
            }
        }
    }
}
