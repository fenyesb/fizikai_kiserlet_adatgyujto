package bluetoothviz.aut.bme.hu;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Handler;
import android.util.Log;

import java.util.List;
import java.util.UUID;

/**
 * Created by Nagy Ákos on 2016. 07. 15..
 */
public class Bluetooth {
    private BluetoothDevice BtDevice = null;
    private BluetoothGatt BtGatt = null;
    private BluetoothAdapter BtAdapter = null;
    private Handler UpdateHandler = new Handler();
    private Visualizer Vis = null;
    private android.content.Context Context = null;

    private BluetoothGattCharacteristic LocalTempChar, RemoteTempChar, PressureChar;

    private int RefreshPeriod = 300;
    private final static UUID UUID_PT_PREASURE_MEASUREMENT = UUID.fromString("2bdb51c1-6987-42bc-9af1-221990841f65");

    public Bluetooth(android.content.Context context, BluetoothAdapter adapter, int period, Visualizer visualizer)
    {
        Context = context;
        BtAdapter = adapter;
        RefreshPeriod = period;
        Vis = visualizer;
    }

    public void Connect()
    {
        if (BtDevice != null)
            BtGatt = BtDevice.connectGatt(Context, false, btleGattCallback);
    }

    public void Disconnect()
    {
        if (BtGatt != null) {
            BtGatt.disconnect();
            BtGatt.close();
        }
    }

    public void Scan()
    {
        if (BtAdapter != null)
            BtAdapter.startLeScan(leScanCallback);
    }

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            if (device.getAddress().equals("00:1E:C0:31:03:73")) {
                BtDevice = device;
                BtAdapter.stopLeScan(leScanCallback);
            }
        }
    };

    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            if (characteristic.getUuid().toString().equals("f7ec04e2-ec8a-4c8b-8688-9a3c4a38c242")) {
                float v = characteristic.getValue()[0];
                v += (float) characteristic.getValue()[1] / 10;
                Vis.AddTemperaturePoint(v);
                Log.d("BluetoothViz", "Local Temperature: " + Double.toString(v));
                if (PressureChar != null)
                    BtGatt.readCharacteristic(PressureChar);
            }
            /*if (characteristic.getUuid().toString().equals("496c882e-d6d3-4e9f-a529-22eab67ed0bf")) {
                float v = characteristic.getValue()[0];
                v += (float) characteristic.getValue()[1] / 10;
                Vis.AddPoint(1, v);
                Log.d("BluetoothViz", "Remote Temperature: " + Double.toString(v));
                if (PressureChar != null)
                    BtGatt.readCharacteristic(PressureChar);
            }*/
            if (characteristic.getUuid().toString().equals("10fb6ed6-5510-4e69-980b-453af4ead084")) {
                float val = characteristic.getValue()[0] * 256;
                byte a = characteristic.getValue()[1];
                if (a < 0)
                    val += (a + 255);
                else
                    val += a;
                Vis.AddPressurePoint((float)val);
                Log.d("BluetoothViz", "Pressure: " + val);
            }
        }

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            // this will get called when a device connects or disconnects
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                BtGatt.discoverServices();
                UpdateHandler.postDelayed(UpdateThread, RefreshPeriod);
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            // this will get called after the client initiates a BluetoothGatt.discoverServices() call
            List<BluetoothGattService> services = BtGatt.getServices();
            for (BluetoothGattService service : services) {
                if (service.getUuid().equals(UUID_PT_PREASURE_MEASUREMENT)) {
                    for (BluetoothGattCharacteristic c : service.getCharacteristics()) {
                        if (c.getUuid().toString().equals("f7ec04e2-ec8a-4c8b-8688-9a3c4a38c242"))
                            LocalTempChar = c;
                        if (c.getUuid().toString().equals("496c882e-d6d3-4e9f-a529-22eab67ed0bf"))
                            RemoteTempChar = c;
                        if (c.getUuid().toString().equals("10fb6ed6-5510-4e69-980b-453af4ead084"))
                            PressureChar = c;
                    }
                }
            }
        }
    };

    private Runnable UpdateThread = new Runnable() {

        public void run() {
            if (LocalTempChar != null)
                BtGatt.readCharacteristic(LocalTempChar);

            UpdateHandler.postDelayed(this, RefreshPeriod);
        }

    };
}
